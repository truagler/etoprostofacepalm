﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication12
{
    class Program
    {
        static void Main(string[] args)
        {
           Random r = new Random();
           int n = Convert.ToInt32(Console.ReadLine());
           int [] a = new int [n];
           for (int i = 0; i < n; i++)
           {
               a[i] = r.Next(-10, 10);
           Console.WriteLine(a[i]);
           }
           Console.WriteLine("============================");
           Console.WriteLine(a[0]);
           Console.WriteLine("============================");
           for (int i = 2; i < n-1; i++)
           {
               Console.WriteLine(a[i]);
           }
           Console.WriteLine("============================");
           Console.WriteLine(a[1]);
           Console.WriteLine("============================");
           for (int i = 3; i < n; i++)
           {
               Console.WriteLine(a[i]);
           }
           Console.WriteLine("============================");
           Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication12
{
    class Program
    {
        static void Main(string[] args)
        {           
            Random r = new Random();
            int n = Convert.ToInt32(Console.ReadLine());  
            int[] Y = new int[n];        
            for (int i = 0; i < n; i++)
            {
                Y[i] = r.Next(0, 10);
                Console.WriteLine(Y[i]);                
            }
            float z;
            float Sum = 0;
            for (int i = 0; i < n; i++)
            {
                if (Math.Abs(Y[i]) < 1)
                   z = Y[i];
                else
                    z = 1 / Y[i];
                Sum += z * z;              
            }
            Console.WriteLine("Сумма=" + Sum);            
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication15
{
    class Program
    {
        static void Main(string[] args)
        {
            var mas = new int[8, 8];
            Random r = new Random();
            int i, j;
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                {
                    mas[i, j] = r.Next(0, 10);
                    Console.Write("{0,3}", mas[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("========================================");
            int sum_mod = 0;
            for (j = 0; j < 8; j++)
            {
                for (i = 0; i < 8; i++)
                {
                    sum_mod += mas[j, i];
                }
            }
            int min = 0;
            for (j = 0; j < 8; j++)
            {
                for (i = 0; i < 8; i++)
                {
                    if (mas[j, i]>sum_mod)
                         min = mas[j, i];
                }
            }
            Console.WriteLine(min);
            Console.ReadKey();
        }
    }
}

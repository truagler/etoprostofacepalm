﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication16
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            var mas = new int[n, n];
            Random r = new Random();
            int i, j;
            for (i = 0; i <n ; i++)
            {
                for (j = 0; j < n; j++)
                {
                    mas[i, j] = r.Next(0, 10);
                    Console.Write("{0,3}", mas[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("========================================");
            double sum1 = 0;
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < n; j++)
                {
                    sum1 +=Math.Abs(mas[i, j]);
                }
            }
            Console.WriteLine(sum1);
            Console.ReadLine();
        }
    }
}

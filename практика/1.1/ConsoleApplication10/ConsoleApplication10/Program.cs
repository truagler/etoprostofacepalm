﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication10
{
    class Program
    {
        static void Main(string[] args)
        {
            var res = F(5);
            Console.WriteLine(res);
            Console.ReadLine();
            }
 
            private static double F(double x)
           {
           return x*CyclicF(x, 1);
           }
 
           private static double CyclicF(double x, int mult)
           {
           if (mult == 256)
           {
           return 256/(x*x);
           }
           return mult/(x*x + CyclicF(x, mult*2));
    
          }
          

        }
    
    }


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication14
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            int n = Convert.ToInt32(Console.ReadLine());
            int[] k = new int[n];
            double factorial = 1;
            double Sum = 0;
            for (int i = 1; i<n; i++)
            {
                k[i] = r.Next(-10,10);
                factorial = (2 * Math.Pow(k[i], 2) + 1) * factorial;
                double z = Math.Pow(-1, k[i]);
                Sum += z * factorial;
            }
            Console.WriteLine(Sum);
            Console.ReadLine();
        }
    }
}

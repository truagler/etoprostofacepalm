﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication11
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();   
            int n = Convert.ToInt32(Console.ReadLine());
            int [] A = new int [n];

            for (int i = 0; i < n; i++)
            {
                A[i] = r.Next(-10,10);
                Console.WriteLine(A[i]);
            }
            Console.WriteLine("===================");
            int[] B = new int[n];
            for (int i = 0; i < n-2; i++)
            {
                B[i] = A[n - 2 + 1] + A[n - 2 + 2];
                Console.WriteLine(B[i]);
            }
            Console.ReadLine();
        }
    }
}
